// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = %DBLDBL_p_DBLDBL ( a , b )
    // Returns dda^ddb
    //
    // Calling Sequence
    // c = a^b
    //
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    if ( b.dd(2)==0 & floor(b.dd(1)) == b.dd(1)) then
      n = b.dd(1)
      c = a^n
    else
      c = exp(b*log(a))
    end
endfunction


