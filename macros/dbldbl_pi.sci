// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = dbldbl_pi ( )
    // Returns a double-double representation of pi.
    //
    // Calling Sequence
    // ddpi = dbldbl_pi()
    //
    // Parameters
    //
    // Description
    // Creates a double-double representation of pi.
    //
    // Examples
    // ddpi = dbldbl_pi ( )
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "dbldbl_pi" , rhs , 0:0 )
    apifun_checklhs ( "dbldbl_pi" , lhs , 0:1 )
    //
    high = 3.1415926535897931D+00
    low = 1.2246467991473532D-16
    z = dbldbl_new(high,low)
endfunction

