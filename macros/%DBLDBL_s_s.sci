// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = %DBLDBL_s_s ( a , b )
    // Computes dd-d
    //
    // Calling Sequence
    // c = dda - db
    //
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    c = dbldbl_ddsub(a.dd,[b;0])
    c = dbldbl_new(c)
endfunction


