// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function c = dbldbl_dddigin ( ca , n )
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 foruble-foruble precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    digits = "0123456789"
    lnca = size(ca,"*")

    d1 = 0.d0

    for i = 1: min(lnca,n)
      k = strindex(digits, ca(i))
      if (k <> []) then
        k = k(1) - 1
        if (k <= 9) then
          d1 = 10 * d1 + k
        end
      end
    end

    c = d1
endfunction

