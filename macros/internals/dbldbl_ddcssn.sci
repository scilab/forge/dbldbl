// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [x,y] = dbldbl_ddcssn ( a )
    //   This computes the cosine and sine of the DD number A and returns the two DD
    //   results in X and Y, respectively.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin



    //   This routine uses the conventional Taylor's series for Sin (s):

    //   Sin (s) =  s - s^3 / 3// + s^5 / 5// - s^7 / 7// ...

    //   where s = t - a * pi / 2 - b * pi / 16 and the integers a and b are chosen
    //   to minimize the absolute value of s.  We can then compute

    //   Sin (t) = Sin (s + a * pi / 2 + b * pi / 16)
    //   Cos (t) = Cos (s + a * pi / 2 + b * pi / 16)

    //   by applying elementary trig identities for sums.  The sine and cosine of
    //   b * pi / 16 are of the form 1/2 * Sqrt {2 +- Sqrt [2 +- Sqrt(2)]}.
    //   Reducing t in this manner insures that -Pi / 32 < s <= Pi / 32, which
    //   accelerates convergence in the above series.

    //>
    //   Uncomment one of the following two sets of lines, preferably the first set
    //   if it is accepted by the compiler.

    //data cs/ &
    //  z'3FEF6297CFF75CB0',  z'3C7562172A361FD3', &
    //  z'3FC8F8B83C69A60B',  z'BC626D19B9FF8D82', &
    //  z'3FED906BCF328D46',  z'3C7457E610231AC2', &
    //  z'3FD87DE2A6AEA963',  z'BC672CEDD3D5A610', &
    //  z'3FEA9B66290EA1A3',  z'3C39F630E8B6DAC8', &
    //  z'3FE1C73B39AE68C8',  z'3C8B25DD267F6600', &
    //  z'3FE6A09E667F3BCD',  z'BC8BDD3413B26456', &
    //  z'3FE6A09E667F3BCD',  z'BC8BDD3413B26456' /
    //data pi/ z'400921FB54442D18',  z'3CA1A62633145C07'/

    cs = [
    9.8078528040323043D-01,   1.8546939997825006D-17, ...
    1.9509032201612828D-01,  -7.9910790684617313D-18, ...
    9.2387953251128674D-01,   1.7645047084336677D-17, ...
    3.8268343236508978D-01,  -1.0050772696461588D-17, ...
    8.3146961230254524D-01,   1.4073856984728024D-18, ...
    5.5557023301960218D-01,   4.7094109405616768D-17, ...
    7.0710678118654757D-01,  -4.8336466567264567D-17, ...
    7.0710678118654757D-01,  -4.8336466567264567D-17
    ];
    cs = matrix(cs,2,2,4)
    pi(1) = 3.1415926535897931D+00
    pi(2) = 1.2246467991473532D-16

    if (a(1) == 0) then
        x(1) = 1
        x(2) = 0
        y(1) = 0
        y(2) = 0
        return
    end

    f(1) = 1
    f(2) = 0

    //   Reduce to between - Pi and Pi.
    s0 = dbldbl_ddmuld (pi, 2 )
    s1 = dbldbl_dddivdd (a, s0 )
    s2 = dbldbl_ddnint (s1 )
    s3 = dbldbl_ddsub (s1, s2 )

    //   Determine nearest multiple of Pi / 2, and within a quadrant, the nearest
    //   multiple of Pi / 16.  Through most of the rest of this subroutine, KA and
    //   KB are the integers a and b of the algorithm above.

    t1 = s3(1)
    t2 = 4 * t1
    ka = round (t2)
    kb = round (8 * (t2 - ka))
    t1 = (8 * ka + kb) / 32
    s1(1) = t1
    s1(2) = 0
    s2 = dbldbl_ddsub (s3, s1 )
    s1 = dbldbl_ddmuldd (s0, s2 )

    //   Compute cosine and sine of the reduced argument s using Taylor's series.

    if (s1(1) == 0) then
        s0(1) = 0
        s0(2) = 0
    else
        s0(1) = s1(1)
        s0(2) = s1(2)
        s2 = dbldbl_ddmuldd (s0, s0 )
        l1 = 0

        while ( %t )
	    l1 = l1 + 1
            if (l1 == 100) then
                error(msprintf("%s: Iteration limit exceeded.","dbldbl_ddcssn"))
            end

            t2 = - (2 * l1) * (2 * l1 + 1)
            s3 = dbldbl_ddmuldd (s2, s1 )
            s1 = dbldbl_dddivd (s3, t2 )
            s3 = dbldbl_ddadd (s1, s0 )
            s0(1) = s3(1)
            s0(2) = s3(2)

            //   Check for convergence of the series.

            if (abs (s1(1)) <= 1d-33 * abs (s3(1))) then
                break
            end
        end
    end

    //   Compute Cos (s) = Sqrt [1 - Sin^2 (s)].

    s1(1) = s0(1)
    s1(2) = s0(2)
    s2 = dbldbl_ddmuldd (s0, s0 )
    s3 = dbldbl_ddsub (f, s2 )
    s0 = dbldbl_ddsqrt (s3 )

    //   Compute cosine and sine of b * Pi / 16.

    kc = abs (kb)
    f(1) = 2.
    if (kc == 0) then
        s2(1) = 1
        s2(2) = 0
        s3(1) = 0
        s3(2) = 0
    else
        s2(1) = cs(1,1,kc)
        s2(2) = cs(2,1,kc)
        s3(1) = cs(1,2,kc)
        s3(2) = cs(2,2,kc)
    end
    if (kb < 0) then
        s3(1) = - s3(1)
        s3(2) = - s3(2)
    end

    //   Apply the trigonometric summation identities to compute cosine and sine of
    //   s + b * Pi / 16.

    s4 = dbldbl_ddmuldd (s0, s2 )
    s5 = dbldbl_ddmuldd (s1, s3 )
    s6 = dbldbl_ddsub (s4, s5 )
    s4 = dbldbl_ddmuldd (s1, s2 )
    s5 = dbldbl_ddmuldd (s0, s3 )
    s1 = dbldbl_ddadd (s4, s5 )
    s0(1) = s6(1)
    s0(2) = s6(2)

    //   This code in effect applies the trigonometric summation identities for
    //   (s + b * Pi / 16) + a * Pi / 2.

    if (ka == 0) then
        x(1) = s0(1)
        x(2) = s0(2)
        y(1) = s1(1)
        y(2) = s1(2)
    elseif (ka == 1) then
        x(1) = - s1(1)
        x(2) = - s1(2)
        y(1) = s0(1)
        y(2) = s0(2)
    elseif (ka == -1) then
        x(1) = s1(1)
        x(2) = s1(2)
        y(1) = - s0(1)
        y(2) = - s0(2)
    elseif (ka == 2 | ka == -2) then
        x(1) = - s0(1)
        x(2) = - s0(2)
        y(1) = - s1(1)
        y(2) = - s1(2)
    end

endfunction


