// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddc = dbldbl_ddadd ( dda, ddb )
    // Compute double-double = double-double + double-double
    // (ddc = dda + ddb).
    // dda(1) represents high-order word, dda(2) represents low-order word. */
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Add two high-order words. */
    s1 = dda(1) + ddb(1);
    bv = s1 - dda(1);
    s2 = ((ddb(1) - bv) + (dda(1) - (s1 - bv)));
    // Add two low-order words. */
    t1 = dda(2) + ddb(2);
    bv = t1 - dda(2);
    t2 = ((ddb(2) - bv) + (dda(2) - (t1 - bv)));
    s2 = s2 + t1;
    // Renormalize (s1, s2) to (t1, s2) */
    t1 = s1 + s2;
    s2 = s2 - (t1 - s1);
    t2 = t2 + s2;
    // Renormalize (t1, t2) */
    ddc(1) = t1 + t2;
    ddc(2) = t2 - (ddc(1) - t1);
endfunction

