// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddsqrt ( a )
    //   This computes the square root of the DD number A and returns the DD result
    //   in B.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin



    //   This subroutine employs the following formula (due to Alan Karp):

    //          Sqrt(A) = (A * X) + 0.5 * [A - (A * X)^2] * X  (approx.)

    //   where X is a double precision approximation to the reciprocal square root,
    //   and where the multiplications A * X and [] * X are performed with only
    //   double precision.

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end


    if (a(1) == 0) then
        b(1) = 0.d0
        b(2) = 0.d0
        return
    end
    t1 = 1 / sqrt (a(1))
    t2 = a(1) * t1
    s0 = dbldbl_dmuld (t2, t2)
    s1 = dbldbl_ddsub (a, s0)
    t3 = 0.5 * s1(1) * t1
    s0(1) = t2
    s0(2) = 0.d0
    s1(1) = t3
    s1(2) = 0.d0
    b = dbldbl_ddadd (s0, s1)
endfunction

