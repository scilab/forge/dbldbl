// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function r = dbldbl_copysign(x,y)
    // Returns r with the magnitude of x and the sign of y.
    //
    // Calling Sequence
    // r = dbldbl_copysign(x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the magnitude of x and the sign of y.
    //
    // Description
    // If y is positive or negative zero, the sign of r is positive.
    //
    // Examples
    //    x = [-5 -4 0 4 5]
    //    y = [-1 0 1 2 3]
    //    r = dbldbl_copysign(x,y)
    //    expected = [-5 4 0 4 5]
    //
    // Bibliography
    //  ISO/IEC 9899 - Programming languages - C, http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    [lhs,rhs]=argn()
    if ( rhs<>2 ) then
        errmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
        "dbldbl_copysign",rhs,1,1)
        error(errmsg)
    end
    if ( size(x) <> size(y) ) then
        errmsg = sprintf(gettext("%s: Size of x and y are different."),..
        "dbldbl_copysign")
        error(errmsg)
    end
    r = zeros(x)
    k = find(y<>0)
    r(k) = abs(x(k)).*sign(y(k))
    k = find(y==0)
    r(k) = abs(x(k))
endfunction

