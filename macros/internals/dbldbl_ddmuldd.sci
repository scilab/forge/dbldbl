// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddc = dbldbl_ddmuldd (dda , ddb )

    //   This routine multiplies DD numbers DDA and DDB to yield the DD product DDC.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    split = 134217729

    //>
    //   On systems with a fused multiply add, such as IBM systems, it is faster to
    //   uncomment the next two lines and comment out the following lines until //>.
    //   On other systems, do the opposite.

    // c11 = dda(1) * ddb(1)
    // c21 = dda(1) * ddb(1) - c11

    //   This splits dda(1) and ddb(1) into high-order and low-order words.

    cona = dda(1) * split
    conb = ddb(1) * split
    a1 = cona - (cona - dda(1))
    b1 = conb - (conb - ddb(1))
    a2 = dda(1) - a1
    b2 = ddb(1) - b1

    //   Multilply dda(1) * ddb(1) using Dekker's method.

    c11 = dda(1) * ddb(1)
    c21 = (((a1 * b1 - c11) + a1 * b2) + a2 * b1) + a2 * b2
    //>
    //   Compute dda(1) * ddb(2) + dda(2) * ddb(1) (only high-order word is needed).

    c2 = dda(1) * ddb(2) + dda(2) * ddb(1)

    //   Compute (c11, c21) + c2 using Knuth's trick, also adding low-order product.

    t1 = c11 + c2
    e = t1 - c11
    t2 = ((c2 - e) + (c11 - (t1 - e))) + c21 + dda(2) * ddb(2)

    //   The result is t1 + t2, after normalization.

    ddc(1) = t1 + t2
    ddc(2) = t2 - (ddc(1) - t1)
endfunction

