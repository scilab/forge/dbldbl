// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = dbldbl_nthroot ( x , n )
    // Computes the n-th root of x.
    //
    // Calling Sequence
    // y = dbldbl_nthroot ( x , n )
    //
    // Parameters
    // x : a double-double, the first point
    // n : a double, integer value
    // y : the n-th root, y=x^(1/n)
    //
    // Description
    //   This computes the N-th root of the DD number A and returns the DD result
    //   in B.  N must be at least one.
    //
    // Examples
    // x = dbldbl_new ( 3 )
    // y = dbldbl_nthroot ( x , 4 )
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs,rhs]=argn()
    apifun_checkrhs ( "dbldbl_nthroot" , rhs , 2:2 )
    apifun_checklhs ( "dbldbl_nthroot" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "dbldbl_nthroot" , x , "x" , 1 , "DBLDBL" )
    apifun_checktype ( "dbldbl_nthroot" , n , "n" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "dbldbl_nthroot" , n , "n" , 2 )
    //
    // Check content
    if ( floor(n) <> n ) then
      error(msprintf("%s: n is not integer.","dbldbl_nthroot"))
    end
    //
    ddb = dbldbl_ddnrt ( x.dd, n )
    y = dbldbl_new(ddb)
endfunction

