// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = %s_r_DBLDBL ( a , b )
    // Divides a double by a double-double
    //
    // Calling Sequence
    // ddc = da / dbb
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))


    dda= [a; 0]
    ddc = dbldbl_dddivdd ( dda, b.dd )
    c = dbldbl_new(ddc)
endfunction


