<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from dbldbl_tostr.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="dbldbl_tostr" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>dbldbl_tostr</refname><refpurpose>Convert to decimal</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   str = dbldbl_tostr ( a )
   str = dbldbl_tostr ( a , fmt)
   str = dbldbl_tostr ( a , fmt, n1)
   str = dbldbl_tostr ( a , fmt, n1, n2)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>a :</term>
      <listitem><para> a double-double</para></listitem></varlistentry>
   <varlistentry><term>fmt :</term>
      <listitem><para> a 1-by-1 matrix of strings, the format. fmt="e" or fmt="f".</para></listitem></varlistentry>
   <varlistentry><term>n1 :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the total number of characters  (default n1=40)</para></listitem></varlistentry>
   <varlistentry><term>n2 :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the total number of characters after the decimal point  (default n2=30)</para></listitem></varlistentry>
   <varlistentry><term>str :</term>
      <listitem><para> a 1-by-1 matrix of strings, the decimal representation of a</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
If no fmt argument is given, converts the DD number A into string form in the string str, with 40 characters.
The format is analogous to the Fortran E format.
   </para>
   <para>
If fmt="e", converts the DD number A to E format, i.e. En1.n2.
B a n1-by-1 matrix of strings, with length 40.
N1 must exceed N2 by at least 8, and N1 must not exceed 80.
N2 must not exceed 30, i.e., not more than 31 significant digits.
   </para>
   <para>
If fmt="f", converts the DD number A to F format, i.e. Fn1.n2.
B a 1-by-1 matrix of strings, with length 40.
N1 must exceed N2 by at least 3, and N1 must not exceed 80.
N2 must not exceed 30.
   </para>
   <para>
If the input arguments are valid, but the data cannot be formatted,
then "*" characters are put in the output string.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
a = dbldbl_new ( 1.23456789123456789 , 1.2345678912345678e-16 );
str = dbldbl_tostr ( a )
str = dbldbl_tostr ( a , "e" )
str = dbldbl_tostr ( a , "f" )
str = dbldbl_tostr ( a , "e", 30 , 20 )
str = dbldbl_tostr ( a , "f", 30 , 20 )

// See with large numbers
a = dbldbl_new ( -1.23456789123456789e-123 )
dbldbl_tostr(a,"f")
dbldbl_tostr(a,"e")
dbldbl_tostr(a,"e",30,15)

// See this in action
a = dbldbl_new ( -1.23456789123456789e-123 )
for n2 = 1 : 30
n1 = n2 + 8;
s = dbldbl_tostr(a,"e",n1,n2);
mprintf("n1=%d,n2=%d, s=%s\n",n1,n2,s);
end

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000</para>
   <para>"A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
