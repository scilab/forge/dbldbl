Double-Double toolbox

Purpose
-------

The goal of this toolbox is to provide a double-double implementation in 
Scilab.
These floating point numbers have a greater precision (but not a greater range) 
that doubles.

This module allows to run the following script:

z1 = dbldbl_new ( 2 )
z2 = dbldbl_new ( 2 )
z1 + z2
z1 * 3
3 * z1
z1 / 3

We can add, subtract, multiply, dividy, compare double-doubles.
We can also compute elementary functions: abs, sin, cos, tan, asin, acos, atan, exp, log, sqrt and power.

This is a Scilab port of David H. Bailey's ddfun90.f.

See the overview in the help provided with this toolbox.

Features
--------

 * dbldlb_new: creates a new double-double number.
 * dbldbl_tostr: Convert to decimal string.
 * dbldbl_nthroot: Computes the n-th root of x.

Dependencies
------------

 * This module depends on the apifun module.
 * This module depends on the helptbx module.

Author
------

2011 - Michael Baudin

Licence
-------

This toolbox is distributed under the CeCILL license.

Bibliography
------------
 * "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
 *  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.

