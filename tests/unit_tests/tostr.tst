// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

a = dbldbl_new ( 1.23456789123456789 , 1.2345678912345678e-16 );
str = dbldbl_tostr(a);
assert_checkequal(str,"1.234567891234568016933781263224E0");
//
a = dbldbl_new ( 1.23456789123456789e-10 , 1.2345678912345678e-26 );
str = dbldbl_tostr(a);
assert_checkequal(str,"1.234567891234568032781714766793E-10");

//
a = dbldbl_new ( 1.23456789123456789 , 1.2345678912345678e-16 );
str = dbldbl_tostr ( a )
assert_checkequal(str,"1.234567891234568016933781263224E0");
//
str = dbldbl_tostr ( a , "e" );
assert_checkequal(str,"1.234567891234568016933781263224E0");
//
str = dbldbl_tostr ( a , "f" );
assert_checkequal(str,"1.234567891234568016933781263224");
//
str = dbldbl_tostr ( a , "e", 30 , 20 );
assert_checkequal(str,"1.23456789123456801693E0");
//
str = dbldbl_tostr ( a , "f", 30 , 20 );
assert_checkequal(str,"1.23456789123456801693");

